import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Home from './Home';
import Buildings from './Buildings';
import Footer from './Footer';

function Main() {
    return (
        <div id="AppWrapper">
            <Router>
                <div className="content-wrapper">
                    <Switch>
                        <Route path="/buildings">
                            <Buildings />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch>
                </div>
                <Footer />
            </Router>
        </div>
      );
}

export default Main;