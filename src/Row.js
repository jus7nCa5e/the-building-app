import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import MenuItem from '@material-ui/core/MenuItem';
import {EmojiTransportation, HomeWork, Apartment, StoreMallDirectory} from '@material-ui/icons';

const buildingImages = [
    {
        value: <EmojiTransportation style={{ fontSize: 50 }}/>,
        label: 'Transportation'
    },
    {
        value: <HomeWork style={{ fontSize: 50 }}/>,
        label: 'HomeWork'
    },
    {
        value: <Apartment style={{ fontSize: 50 }}/>,
        label: 'Apartment'
    },
    {
        value: <StoreMallDirectory style={{ fontSize: 50 }}/>,
        label: 'Store'
    },
]

function AddRowModal({open, data, onSubmit, onCancel, onClose}) {
    const [newData, setNewData] = React.useState({});

    React.useEffect(()=>{
        if(data){
            setNewData(data);
        }
    }, [data])

    const handleSubmit = (ev) => {
        ev.preventDefault();
        onSubmit(newData);
    }

    return (
        <div className="modal-form">
            <Dialog open={open} onClose={onClose} fullWidth={true} maxWidth="lg" aria-labelledby="form-dialog-title">
                <form onSubmit={handleSubmit} className="px-3">
                    <TextField
                        defaultValue={data && data.id}
                        onInput={ e=> {setNewData(Object.assign(newData, {id: e.target.value}))}}
                        id="id"
                        label="ID"
                        type="number"
                        margin="normal"
                        className="me-3"
                        required
                        // error={data === ""}
                        // helperText="cannot be empty!"
                    />
                    <TextField
                        defaultValue={data && data.name}
                        onInput={ e=> {setNewData(Object.assign(newData, {name: e.target.value}))}}
                        margin="normal"
                        id="name"
                        label="Name"
                        type="text"
                        className="me-3"
                        required
                    />
                    <TextField
                        defaultValue={data && data.area}
                        onInput={ e=> {setNewData(Object.assign(newData, {area: e.target.value}))}}
                        margin="normal"
                        id="area"
                        label="Area"
                        type="number"
                        className="me-3"
                        required
                    />
                    <TextField
                        defaultValue={data && data.location}
                        onInput={ e=> {setNewData(Object.assign(newData, {location: e.target.value}))}}
                        margin="normal"
                        id="location"
                        label="Location"
                        type="text"
                        className="me-3"
                    />
                    <TextField
                        defaultValue={data && data.image}
                        onChange={ e=> {setNewData(Object.assign(newData, {image: e.target.value}))}}
                        margin="normal"
                        id="image"
                        label="Image"
                        type="text"
                        style={{ width: 180 }}
                        select
                        >
                        {buildingImages.map((option, index) => (
                            <MenuItem key={index} value={option.value}>
                              {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                    <DialogActions>
                        <Button onClick={onCancel} color="primary">
                            Cancel
                        </Button>
                        <Button type="submit" color="primary">
                            Ok
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );
}

export default AddRowModal
