import {Link} from "react-router-dom";

function Footer(){
    return (
        <footer className="bg-primary">
            <div className="container py-4 d-flex align-items-center justify-content-between">
                <ul className="nav">
                    <li className="nav-item">
                        <Link to="/" className="nav-link text-uppercase text-light">Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/buildings" className="nav-link text-uppercase text-light">Buildings</Link>
                    </li>
                </ul>
                <span className="text-light">
                    Fusce fermentum odio nec Project
                </span>
            </div>
        </footer>
    )
}

export default Footer;