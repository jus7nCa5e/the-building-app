import React from 'react';
import {Delete, Edit} from '@material-ui/icons';

import AddRowModal from './Row';
import {AddCircle} from '@material-ui/icons';


function Table({list}) {

    const[dataTable, setDataTable] = React.useState(list);
    const [openAdd, setOpenAdd] = React.useState(false);
    const [openEdit, setOpenEdit] = React.useState(false);
    const [indexInEdit, setIndexInEdit] = React.useState(null);
    
    const handleDeleteRow = (item) => {
        setDataTable(dataTable.filter((v, index)=>index != item))
    };

    const updateRow = (data, index) => {
        dataTable[index] = data;
        setDataTable(dataTable.slice());
        setOpenEdit(false);
    }

    const handleOpenEdit = index => {
        setIndexInEdit(index);
        setOpenEdit(true);
    }

    const handleCloseEdit = () => {
        setOpenEdit(false);
    }

    const handleCloseAdd = () => {
        setOpenAdd(false);        
    }

    const handleOpenAdd = () => {
        setOpenAdd(true);        
    }

    const handleAdd = data => { 
        setDataTable(dataTable.concat([data]))
        setOpenAdd(false);
    }

    const handleEdit = data => {
        updateRow(data, indexInEdit);
    }

    return (
        <div className="container">
            <h1 className="pb-2 ps-3">Aenean posuere tortor</h1>
            <table id="Buildings" className="w-100 text-center">
                <thead className="pm-2">
                    <tr className="text-secondary text-uppercase">
                        <th className="border-top border-bottom border-start">ID</th>
                        <th className="">Name</th>
                        <th className="">Area</th>
                        <th className="">Location</th>
                        <th className="border-end border-top">Image</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataTable.map((item, index) => (    
                            <tr key={index} className="py-5">
                                <td className="">{item.id}</td>
                                <td>{item.name}</td>
                                <td>{item.area}</td>
                                <td>{item.location}</td>
                                <td className="border-end">{item.image}</td>
                                <td className="text-start" style={{width: 100}}>
                                    <button onClick={() => handleDeleteRow(index)} className="btn bg-white border-0">
                                        <Delete className="text-danger"/>
                                    </button>
                                    <button onClick={()=>handleOpenEdit(index)} className="btn bg-white border-0">
                                        <Edit className="text-success"/>
                                    </button>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
            <div className="text-end pt-2">
                <button onClick={handleOpenAdd} className="btn bg-white border-0 add-row">
                     <AddCircle fontSize="large" className="text-primary"/>
                </button>
                <AddRowModal open={openAdd} onSubmit={handleAdd} onCancel={handleCloseAdd}/>
                <AddRowModal data={dataTable[indexInEdit]} open={openEdit} onSubmit={handleEdit} onCancel={handleCloseEdit}/>
            </div>
        </div>
    );
}

export default Table;
