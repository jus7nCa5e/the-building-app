import React from 'react';
import {EmojiTransportation, HomeWork, Apartment, AccountCircle} from '@material-ui/icons';
import Table from './Table';

const initialList = [
    {
        "id": 12,
        "name": "Qchuk",
        "area": "750",
        "location": "Plovdiv, Kapana 78 Str",
        "image": <HomeWork style={{ fontSize: 50 }}/>,
    },
    {
        "id": 28,
        "name": "White horse",
        "area": "1200",
        "location": "Sofia, Central 7 Str",
        "image": <EmojiTransportation style={{ fontSize: 50 }}/>,
    },
    {
        "id": 4,
        "name": "Royalty",
        "area": "1800",
        "location": "Sofia, Vrazhdebna 59 Str",
        "image": <Apartment style={{ fontSize: 50 }}/>,
    },
];

function Home() {
    return (
        <div className="">
            <header className="bg-primary">
                <div className="nav container ps-2 py-3 d-flex justify-content-between">
                    <h4 className="nav-link disabled mb-0 d-flex text-light align-items-center">Welcome, Duis Praesent</h4>
                    <AccountCircle className="me-3 text-light" style={{ fontSize: 50 }} />
                </div>
            </header>
            <section className="pt-5">
                <Table list={initialList} />
            </section>
        </div>
    );
} 

export default Home;