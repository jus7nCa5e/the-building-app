import React from 'react';

function Home() {
    return(
        <div className="container pt-5">
            <h1 className="font-weight-bold display-3 text-center text-secondary text-uppercase pb-3">BUILDINGS</h1>
            <p>
                &nbsp; &nbsp; Nunc sed turpis. Fusce ac felis sit amet ligula pharetra condimentum. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. Phasellus dolor.

                Phasellus accumsan cursus velit. Duis vel nibh at velit scelerisque suscipit. Morbi ac felis. Curabitur ullamcorper ultricies nisi. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc.
                
                Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Vivamus consectetuer hendrerit lacus. Donec vitae orci sed dolor rutrum auctor. Nullam accumsan lorem in dui. Nunc nec neque.
                
                Ut id nisl quis enim dignissim sagittis.. Cras non dolor. Nullam cursus lacinia erat. Quisque ut nisi.
                
                Cras non dolor. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Mauris sollicitudin fermentum libero. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Aenean imperdiet.</p>
            <p>
                &nbsp; &nbsp; Praesent venenatis metus at tortor pulvinar varius. Morbi vestibulum volutpat enim. Etiam imperdiet imperdiet orci. Aliquam erat volutpat. Vestibulum eu odio.

                Ut varius tincidunt libero. Donec sodales sagittis magna. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Fusce vel dui. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.

                Morbi ac felis. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Etiam ultricies nisi vel augue. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Nulla porta dolor.
            </p>
        </div>
    )
}

export default Home;